# Minimal admin

Un cms minimal permettant de créer des pages avec sections et blocks

## Technologies utilisées

- Slim Framework 4
- AdminLTE

## Installation

### Prérequis

- Un serveur apache
- composer installé sur le serveur

### Procédure

1. Cloner le répertoire
2. lancer la commande `composer install` (selon la version de composer installé ça peut aussi être `composer.phar install`)
3. Créer une base de données vide
4. Copier le fichier `.env.example` et le renommer `.env`
5. Modifier le nouveau fichier `.env` avec les données de la nouvelle base de données
6. Importer le fichier `dump.sql` dans la base
7. Et voilà !