<?php
require 'vendor/autoload.php';
include_once('.env');

use Password;
use Api;
use Pages;
use Sections;
use Tokens;
use Newspapers;
use Articles;

session_start(); //by default requires session storage

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

if (DB_DBNAME) {
	$config['db']['host']   = DB_HOST;
	$config['db']['user']   = DB_USER;
	$config['db']['pass']   = DB_PASS;
	$config['db']['dbname'] = DB_DBNAME;
}

// Create app
$app = new \Slim\App(['settings' => $config]);

// Get container
$container = $app->getContainer();

if (DB_DBNAME) {
	$container['db'] = function ($c) {
		$db = $c['settings']['db'];
		$pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
			$db['user'], $db['pass']);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		return $pdo;
	};
}


// Register provider
$container['flash'] = function () {
	return new \Slim\Flash\Messages();
};

$container['twigPath'] = 'templates';

$container['MW'] = new \stdClass();
$container['MW']->isLoggedMW = function ($request, $response, $next) {
	Password::init($this->db);
	if (Password::isLogged()) {
		$response = $next($request, $response);
	} else {
		$response = $response->withRedirect($this->router->pathFor('admin'));
	}

	return $response;
};
$container['MW']->isTokenRoute = function ($request, $response, $next, $args) {
	$args = $request->getAttribute('routeInfo')[2];
	$token = isset($args['token']) ? $args['token'] : false;
	Tokens::init($this->db);
	if ($token && Tokens::check($token)) {
		$response = $next($request, $response);
	} else {
		$response = $response
		->withStatus(401)
		->withHeader('Content-Type', 'text/html')
		->write('Not authenticated');
	}

	return $response;
};

// Register component on container
$container['view'] = function ($container) {
	$view = new \Slim\Views\Twig($container['twigPath'], [
		'cache' => 'cache',
		'auto_reload' => true,
		'debug' => true
	]);

	// Instantiate and add Slim specific extension
	$router = $container->get('router');
	$uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
	$view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

	return $view;
};

$app->group('/admin', function() use ($app) {
	$this->get('', function ($request, $response, $args) {
		Password::init($this->db);
		Pages::init($this->db);
		$slug = 'empty';
		$firstPage = Pages::getFirst();
		if ($firstPage) {
			$slug = $firstPage->slug;
		}
		if (Password::isLogged()) {
			return $response->withRedirect($this->router->pathFor('admin_page', ['page' => $slug]));
		} else {
			return $this->view->render($response, 'login.html.twig', [
				'messages' => $this->flash->getMessages()
			]);
		}
	})->setName('admin');


	$this->post('', function ($request, $response, $args) {
		Password::init($this->db);
		$data = $request->getParsedBody();
		$isOk = true;

		if (!isset($data['password'])) {
			$isOk = false;
			$this->flash->addMessage('error', 'Aucun mot de passe n\'a été fourni');
		} elseif (!Password::test(filter_var($data['password'], FILTER_SANITIZE_STRING))) {
			$isOk = false;
			$this->flash->addMessage('error', 'Le mot de passe ne correspond pas');
		}

		if ($isOk) {
			$cookie = Password::setCookie();
		}

		return $response->withRedirect($this->router->pathFor('admin'));
	})->setName('login');

	$this->post('/new_password', function ($request, $response, $args) {
		Password::init($this->db);
		$newPassword = Password::reinitLogin();
		if ($newPassword) {
			if(Password::sendPasswordEmail($newPassword)) {
				$this->flash->addMessage('info', 'Un nouveau mot de passe vous a été envoyé par email, vous le receverez d\ici peu de temps !');
			}
		}

		return $response->withRedirect($this->router->pathFor('admin'));
	})->setName('forget_password');

	$this->group('/private', function() {

		$this->get('/pages/{page}/{section}', function ($request, $response, $args) {
			Pages::init($this->db);
			$page = Pages::getBySlug($args['page']);

			return renderAdminPage($this, $response, $page, $args['section']);
		})
		->setName('admin_page_specific')
		;

		$this->get('/pages/{page}', function ($request, $response, $args) {
			Pages::init($this->db);
			$page = Pages::getBySlug($args['page']);

			return renderAdminPage($this, $response, $page);
		})
		->setName('admin_page')
		;

		$this->get('/sections/{section}', function ($request, $response, $args) {
			Pages::init($this->db);
			Sections::init($this->db);
			$section = Sections::get($args['section']);

			return $this->view->render($response, 'admin.section.html.twig', [
				'pages' => Pages::getAll(),
				'pageActive' => $section ? $section->getPage() : false,
				'sectionActive' => $section,
				'messages' => $this->flash->getMessages()
			]);
		})
		->setName('admin_section')
		;
	})->add($app->getContainer()->MW->isLoggedMW);

});

$app->group('/api', function() use ($app) {
	$this->group('/private', function() {
		$this->post('/sort', function ($request, $response, $args) {
			Api::init($this->db);
			$parsedBody = $request->getParsedBody();
			$test = Api::sort($parsedBody['objectClass'], $parsedBody['orders']);
			return $response;
		})
		->setName('api_sort')
		;

		$this->post('/post', function ($request, $response, $args) {
			Api::init($this->db);

			$parsedBody = $request->getParsedBody();
			$class = $parsedBody['objectClass'];
			unset($parsedBody['objectClass']);
			$id = $parsedBody['id'];
			unset($parsedBody['id']);
			$redirect = $parsedBody['redirect'];
			unset($parsedBody['redirect']);

			$action = '';
			if (isset($parsedBody['action'])) {
				$action = $parsedBody['action'];
				unset($parsedBody['action']);
			}

			if ($action == 'delete') {
				if (Api::delete($class, $id)) {
					$this->flash->addMessage('success', 'La suppression a été effectuée');
				} else {
					$this->flash->addMessage('error', 'Erreur lors de la suppression');
				}
			} elseif ($id == 'new') {
				if (Api::post($class, $parsedBody)) {
					$this->flash->addMessage('success', 'La création a été effectuée');
				} else {
					$this->flash->addMessage('error', 'Erreur lors de la création');
				}
			} else {
				if (Api::put($class, $id, $parsedBody)) {
					$this->flash->addMessage('success', 'La modification a été effectuée');
				} else {
					$this->flash->addMessage('error', 'Erreur lors de la modification');
				}
			}

			return $response->withRedirect($redirect);
		})
		->setName('api_post')
		;
	})->add($app->getContainer()->MW->isLoggedMW);

	$this->group('/public', function() {
		/**
		 * Pour générer un nouveau token, il faut poster un objet
		 * avec la variable 'password'
		 * ex: {
		 * 	"password": "blablabla"
		 * }
		 */
		$this->post('/tokens', function ($request, $response, $args) {
			Password::init($this->db);
			$parsedBody = $request->getParsedBody();
			$password = isset($parsedBody['password']) ? $parsedBody['password'] : false;

			if (!Password::test($password)) {
				return $response
				->withStatus(401)
				->withHeader('Content-Type', 'text/html')
				->write('Not authenticated, can\'t generate a token')
				;
			}

			Tokens::init($this->db);
			$token = Tokens::generate();

			return $response->withJson($token);
		})
		->setName('public_tokens')
		;
	});

	$this->group('/{token}', function() {
		$this->post('/insert/into/{table}', function ($request, $response, $args) {
			Api::init($this->db);
			$parsedBody = $request->getParsedBody();
			$post = Api::post($args['table'], $parsedBody, true);

			return $response->withJson($post);
		})
		->setName('token_posts')
		;
	})
	->add($app->getContainer()->MW->isTokenRoute)
	;
});

$app->get('/sitemap.xml', function ($request, $response, $args) {
	Pages::init($this->db);

	$response = $response->withHeader('Content-type', 'applicationx/ml');
	$pages = Pages::getAll();
	$firstPage = Pages::getFirst();

	$pagesInTheSitemap = [];
	foreach ($pages as $page) {
		if ($page->slug != $firstPage->slug) {
			$pagesInTheSitemap[] = $page;
		}
	}
	return $this->view->render($response, 'sitemap.xml.twig', [
		'pages' => $pagesInTheSitemap,
		'domain' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST']
	]);
})
->setName('sitemap')
;

$app->get('/', function ($request, $response, $args) {
	Pages::init($this->db);
	$firstPage = Pages::getFirst();

	return renderPage($this, $response, $firstPage);
})->setName('home');


$app->get('/{page}', function ($request, $response, $args) {
	Pages::init($this->db);
	$page = Pages::getBySlug($args['page']);
	$firstPage = Pages::getFirst();
	if (!$page || ($page->slug == $firstPage->slug)) {
		return $response->withRedirect($this->router->pathFor('home'));
	}
	return renderPage($this, $response, $page);
})
->setName('page')
;

$app->get('/{page}/{section}', function ($request, $response, $args) {
	Pages::init($this->db);
	Sections::init($this->db);
	$page = Pages::getBySlug($args['page']);
	$section = Sections::getBySlug($page->id, $args['section']);

	return renderPage($this, $response, $page, $section);
})
->setName('page_section')
;

function renderPage($app, $response, $page, $section = null) {
	$template = 'index.html.twig';

	$pageName = $page->slug;
	if ($section) {
		$pageName .= '_'.$section->slug;
	}
	if (file_exists(dirname(__FILE__).'/'.$app['twigPath'].'/pages/'.$pageName.'.html.twig')) {
		$template = 'pages/'.$pageName.'.html.twig';
	}

	$values = [];
	if (function_exists($pageName)) {
		$function = $pageName;
		$values = $function($app, $values);
	}

	$values['pages'] = Pages::getAll();
	$values['pageActive'] = $page;
	$values['sectionActive'] = $section;
	$values['messages'] = $app->flash->getMessages();

	return $app->view->render($response, $template, $values);
}

function renderAdminPage($app, $response, $page, $sectionSlug = null) {
	$template = 'admin.page.html.twig';

	$pageName = $page->slug;
	if ($sectionSlug) {
		$pageName .= '_'.$sectionSlug;
	}

	if (file_exists(dirname(__FILE__).'/'.$app['twigPath'].'/admin/'.$pageName.'.html.twig')) {
		$template = 'admin/'.$pageName.'.html.twig';
	}
	$values = [];

	$pageName = 'admin_'.$pageName;
	if (function_exists($pageName)) {
		$function = $pageName;
		$values = $function($app, $values);
	}

	Pages::init($app->db);

	$values['pages'] = Pages::getAll();
	$values['pageActive'] = $page;
	$values['messages'] = $app->flash->getMessages();

	return $app->view->render($response, $template, $values);
}

function admin_newspapers($app, $values) {
	Newspapers::init($app->db);
	$values['newspapers'] = Newspapers::getAll();

	return $values;
}

function admin_newspapers_new($app, $values) {
	Articles::init($app->db);
	$values['articles'] = Articles::getAll();

	return $values;
}

function admin_newspapers_archived($app, $values) {
	Articles::init($app->db);
	$values['articles'] = Articles::getAll('archived');

	return $values;
}

// Run app
$app->run();
