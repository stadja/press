
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('to_be_selected','selected','archived') NOT NULL DEFAULT 'to_be_selected',
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `url` varchar(255) DEFAULT NULL,
  `tags` varchar(255) NOT NULL,
  `img_header` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (2,'to_be_selected','Les deux oursons nÃ©s cet hiver dans les PyrÃ©nÃ©es sont probablement morts','La nature est parfois cruelle. A peine sortis du ventre de leur mÃ¨re et de leur taniÃ¨re, deux oursons, nÃ©s en janvier en pays Toy, dans les Hautes-PyrÃ©nÃ©es, avaient Ã©tÃ© aperÃ§us pour la premiÃ¨re fois le 17 avril par un agent de lâ€™Office national de la chasse et de la faune sauvage (ONCFS).','https://www.lemonde.fr/planete/article/2019/06/05/les-deux-oursons-nes-cet-hiver-dans-les-pyrenees-sont-probablement-morts_5471845_3244.html','','https://img.lemde.fr/2019/06/05/0/0/4009/2673/688/0/60/0/8b28ca2_4Of4TRbJMDf-smgx3M9geV3g.jpg',NULL,'2019-05-05 13:35:35','2019-06-05 15:28:17',NULL,4),(4,'to_be_selected','HÃ´pitalÂ : urgences, l\'heure de la nÃ©gociationÂ ?','Ils vont venir, puis sâ€™installer devant le ministÃ¨re de la SantÃ©.','https://www.liberation.fr/france/2019/06/05/hopital-urgences-l-heure-de-la-negociation_1731818','','https://medias.liberation.fr/photo/1225263-000_1fn7vs.jpg?modified_at=1559741734&width=960',NULL,'2019-06-05 14:45:07','2019-06-05 15:28:17',NULL,0),(5,'to_be_selected','A fast, privacy-focused commenting platform','Commento Pricing Login Signup A fast, privacy-focused commenting platform Embed Commento in your website to foster discussion and improve engagement.','https://commento.io/','','https://cdn.commento.io/images/demo.png',NULL,'2019-06-05 15:21:45','2019-06-05 15:28:17',NULL,5),(6,'to_be_selected','Investi par la grande distribution: lâ€™eldoraBIO','Â«Pas de tomate bio en hiver ! Non Ã  lâ€™industrialisation de la bio !Â» Voici le mot dâ€™ordre dâ€™une pÃ©tition lancÃ©e mardi soir par la FÃ©dÃ©ration nationale dâ€™agriculture biologique (Fnab), la Fondation Nicolas-Hulot pour la nature et lâ€™homme, Greenpeace et le RÃ©seau Action-Climat et rel','https://www.liberation.fr/planete/2019/05/28/investi-par-la-grande-distribution-l-eldorabio_1730298','','https://medias.liberation.fr/photo/1223216-agriculture-sous-serre-illustration-eclairage.jpg?modified_at=1559071572&width=960',NULL,'2019-06-05 15:22:44','2019-06-05 15:28:17',NULL,1),(7,'to_be_selected','Vous ne savez pas ce qu\'est le Â«male gazeÂ»? Il suffit de voir le film de Kechiche','Des fesses, des fesses, des fesses. Des gros culs, des petits culs, des moyens culs. Des culs Ã  la plage, des culs en boÃ®te, des culs aux toilettes. Mektoub My Love: Intermezzo est trÃ¨s clairement un film de cul(s).','https://www.slate.fr/story/177657/cinema-festival-cannes-mektoub-my-love-intermezzo-abdellatif-kechiche-male-gaze','','http://www.slate.fr/sites/default/files/screen_shot_2019-05-02_at_15.13.53.png',NULL,'2019-06-05 15:22:45','2019-06-05 15:28:17',NULL,3),(8,'to_be_selected','Angot, aprÃ¨s un temps de rÃ©flexion','Exit Angot. Mais attention. Ca n\'a rien Ã  voir avec sa sortie nÃ©gationniste sur la traite des Noirs (900 signalements au CSA, voir le matinaute d\'hier).','https://www.arretsurimages.net/chroniques/le-matinaute/angot-apres-un-temps-de-relfexion','','https://api.arretsurimages.net/api/public/media/angot-tweet-ruquier/action/show?format=public&t=2019-06-05T09:26:48+02:00',NULL,'2019-06-05 15:27:35','2019-06-05 15:28:17',NULL,2);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`press`@`%`*/ /*!50003 TRIGGER `before_update_article` BEFORE UPDATE ON `article` FOR EACH ROW SET new.updated_at = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `image` text,
  `content` text,
  `title` varchar(255) DEFAULT NULL,
  `link_title` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `external` tinyint(4) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`),
  CONSTRAINT `block_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=565 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `block` WRITE;
/*!40000 ALTER TABLE `block` DISABLE KEYS */;
/*!40000 ALTER TABLE `block` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `newspaper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newspaper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `img_header` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `newspaper` WRITE;
/*!40000 ALTER TABLE `newspaper` DISABLE KEYS */;
/*!40000 ALTER TABLE `newspaper` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`press`@`%`*/ /*!50003 TRIGGER before_update_post
  BEFORE UPDATE ON `newspaper` FOR EACH ROW
  SET new.updated_at = CURRENT_TIMESTAMP */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `description` text,
  `sort` int(11) DEFAULT '0',
  `fa_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (15,'accueil','Accueil','',0,'home'),(23,'newspapers','Revues de presse','',1,'newspaper-o');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `section_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES ('password','$2y$10$4n3b3U/fmmatXPAE//7Q..WX9btvybR.icY983mXuPFJViU3Nklci');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES (4,'5c414ca2-877a-11e9-b5da-00163eb83e1a',NULL,'2019-06-05 10:12:01'),(5,'8c830be2-8786-11e9-b5da-00163eb83e1a',NULL,'2019-06-05 11:39:16'),(6,'90d4c2c1-8786-11e9-b5da-00163eb83e1a',NULL,'2019-06-05 11:39:23'),(7,'c09f73a6-8786-11e9-b5da-00163eb83e1a',NULL,'2019-06-05 11:40:43'),(8,'c4972ed0-8786-11e9-b5da-00163eb83e1a',NULL,'2019-06-05 11:40:50');
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`press`@`%`*/ /*!50003 TRIGGER before_insert_token
  BEFORE INSERT ON token 
  FOR EACH ROW
  SET new.token = uuid() */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

