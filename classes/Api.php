<?php

class Api {
	static $pdo;
	static $isInited = false;

	static function init($pdo) {
		self::$pdo = $pdo;
		self::$isInited = true;
	}

	static function sort($class, $order) {
		$when = '';
		$order = array_values(array_filter($order));
		foreach($order as $o => $id) {
			$when .= "WHEN id = $id THEN $o ";
		}
		$sql = "UPDATE $class SET sort = CASE $when END WHERE id IN (" . implode(',', $order).')';
		$pdo = self::$pdo;
		$requete_gamme = $pdo->prepare($sql);
		return $requete_gamme->execute();
	}

	static function post($class, $data, $getLastInserted = false) {
		$pdo = self::$pdo;
		$sql = "INSERT INTO ".$class." (";
		$index = 0;
		foreach ($data as $key => $value) {
			if ($index > 0) {
				$sql .= ', ';
			}
			$sql .= '`'.$key.'`';
			$index++;
		}
		$sql .= ") VALUES (";
		$index = 0;
		foreach ($data as $key => $value) {
			if ($index > 0) {
				$sql .= ', ';
			}
			$sql .= $pdo->quote($value);
			$index++;
		}
		$sql .= ");";

		$requete_gamme = $pdo->prepare($sql);
		$post = $requete_gamme->execute();

		if ($getLastInserted) {
			$sql = "SELECT * FROM ".$class." WHERE id = ".self::$pdo->lastInsertId();
			$sql = self::$pdo->prepare($sql);
			$sql->execute();
			$post = $sql->fetch();
		}

		return $post;
	}

	static function put($class, $id, $data) {
		$pdo = self::$pdo;
		$sql = "UPDATE ".$class." SET ";
		$index = 0;
		foreach ($data as $key => $value) {
			if ($index > 0) {
				$sql .= ', ';
			}
			$sql .= '`'.$key.'` = '.$pdo->quote($value);
			$index++;
		}
		$sql .= " WHERE id = ".$id;

		$requete_gamme = $pdo->prepare($sql);
		return $requete_gamme->execute();
	}

	static function delete($class, $id) {
		$pdo = self::$pdo;
		$sql = "DELETE FROM ".$class." WHERE id = ".$id;
		$requete_gamme = $pdo->prepare($sql);
		return $requete_gamme->execute();
	}
}
