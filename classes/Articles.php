<?php
use Article;

class Articles {
	static $pdo;
	static $isInited = false;

	static function init($pdo) {
		self::$pdo = $pdo;
		self::$isInited = true;
	}

	static function getAll($status = 'to_be_selected') {
		$sql = "SELECT * FROM  `article` WHERE status = '".$status."' ORDER BY sort ASC, created_at ASC ";
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$articles = array();
		while($article = $sql->fetch()) {
			$articles[] = new Article(self::$pdo, $article);
		}

		return $articles;
	}

	static function getBySlug($slug) {
		$sql = "SELECT * FROM  `article` WHERE slug = ".self::$pdo->quote($slug);
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$articles = array();
		$article = $sql->fetch();
		if ($article) {
			$article = new Article(self::$pdo, $article);
		}

		return $article;
	}

	static function get($id) {
		$sql = "SELECT * FROM  `article` WHERE id = ".self::$pdo->quote($id);
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$articles = array();
		$article = $sql->fetch();
		if ($article) {
			$article = new Article(self::$pdo, $article);
		}

		return $article;
	}

	static function getFirst() {
		$sql = "SELECT * FROM  `article` ORDER BY sort ASC";
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$articles = array();
		$article = $sql->fetch();
		if ($article) {
			$article = new Article(self::$pdo, $article);
		}

		return $article;
	}
}
