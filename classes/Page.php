<?php
use Section;

class Page {
	protected $pdo;
	protected $sections;

    public function __construct($pdo, $data) {
		$this->pdo = $pdo;
        foreach($data as $key => $value) {
           $this->$key = $value;
        }
    }

    public function getSections() {
    	if (!$this->sections) {
			$sql = "SELECT * FROM `section` WHERE page_id = ".$this->id." ORDER BY sort ASC";
			$sql = $this->pdo->prepare($sql);
			$sql->execute();
			$this->sections = array();
			while($section = $sql->fetch()) {
				$this->sections[] = new Section($this->pdo, $section);
			}
    	}

    	return $this->sections;
    }

}
