<?php
use Newspaper;

class Newspapers {
	static $pdo;
	static $isInited = false;

	static function init($pdo) {
		self::$pdo = $pdo;
		self::$isInited = true;
	}

	static function getAll() {
		$sql = "SELECT * FROM  `newspaper` ORDER BY published_at DESC ";
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$newspapers = array();
		while($newspaper = $sql->fetch()) {
			$newspapers[] = new Newspaper(self::$pdo, $newspaper);
		}

		return $newspapers;
	}

	static function getBySlug($slug) {
		$sql = "SELECT * FROM  `newspaper` WHERE slug = ".self::$pdo->quote($slug);
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$newspapers = array();
		$newspaper = $sql->fetch();
		if ($newspaper) {
			$newspaper = new Newspaper(self::$pdo, $newspaper);
		}

		return $newspaper;
	}

	static function get($id) {
		$sql = "SELECT * FROM  `newspaper` WHERE id = ".self::$pdo->quote($id);
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$newspapers = array();
		$newspaper = $sql->fetch();
		if ($newspaper) {
			$newspaper = new Newspaper(self::$pdo, $newspaper);
		}

		return $newspaper;
	}

	static function getFirst() {
		$sql = "SELECT * FROM  `newspaper` ORDER BY sort ASC";
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$newspapers = array();
		$newspaper = $sql->fetch();
		if ($newspaper) {
			$newspaper = new Newspaper(self::$pdo, $newspaper);
		}

		return $newspaper;
	}
}
